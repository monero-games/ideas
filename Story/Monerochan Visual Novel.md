# Intro

You're a boy and live in a small village with your parents.
Your parents are working a lot at the fields and don't have much time for you.
But your mom is still very nice to you.
You also have to leave.

You always wanted to go to the nearby city.
It seems like a magical place to you, where you don't need to work.

You already got some device, which you can use to browse the city network.
You've seen a lot of pictures of streamers.
And you fell in love with some popular girl around your age, whose profile was called Monerochan.
If you go to the city, you are going to find her.

Your parents didn't want you to leave.
But you're old enough now and they can't stop you anymore.

So you leave your home and enter the city for the first time.

# Chapter 1: The big City

You enter the city and are fascinated and disgusted at the same time.
There are a lot of people. They wear weird clothes. Some of them don't wear any.
And some of them even don't look like humans.

You try to find out how to get some place to sleep there.
You go to some public PC, which scans your face and lets you choose a free apartment.
You are also allowed to use a shared flat.

At your flat, you spend a lot of time browsing the network.
The connection is much better now, and you watch full streams, mostly of that girl.
You mostly watch her because she's attractive and because of her personality.

You also like to go out and explore the city.
It's a weird place and many things, which have been unthinkable in your village are totally normal here: Slaves, fights to death, public sex.
People probably started to get bored of normal activities.
You are not sure, what you should think about that.

You always watch the streams of that girl. You even were able to get responses from her in chats.
But after some time she starts to talk about weird stuff. She criticizes the city, how they opress everyone, how live could be better without ownership restrictions, how some elite is controlling everything and knows everything about everyone, and more.
You want to believe her because you like her but you can't.

In one stream, she told her viewers to go out and see it for themselves.
You should go to the forest and the villages and see, what they do to the people there.

After that, she never streams again.
It's very difficult to find recordings of them.
She also does not answer any chats anymore.
It's almost as if she never existed.

# Chapter 2: Vacation

You decide to follow her advice and go to the forest.
It's recommended to do a guided tour, so you do that in order to attract no attention.

You get equiped with a gun to stun everyone who becomes dangerous, and with some almost invisible gear to protect you from shots of this weapon.
Then you visit a small settlement, where everything seems fine.
All of them look happy and healthy and almost have no clothes.

You talk to them and feed them with food from the city.
Most of them don't seem too intelligent. They are very naive about the world.
But they also seem to be pretty active and well trained. They run around a lot and some of them climb trees.

They also tell you, that you're allowed to take them with you and use them as slaves.
Life as slaves in the city is still better than their primitive life here.
And you should always treat them properly.
You think about doing that.

Then you want to explore the forest on your own.
You go away from the settlement and then put off your gear and put your weapon down, since you heard, they are tracking you in case you get lost.

After some time you reach a new settlement.
The people there seem very hostile and most of them are scared.

You try to be nice and you kind of get accepted.
Some of them tell you, people from the city have been very evil.
They were treated like garbage, many of them were raped, even children, and some were even killed.
They couldn't do anything against them without any weapons.

When they realize, you are alone and don't have any gear or weapon with you, they start to attack you and want to kill you.
But you are able to convince them, that you want to help them.

You find a few friends there and have a good time.
After some time you want to go back to the city.
You take one of your new friends with you.

Back at the city you tell them, you are back and lost your gear.
Your new friend is registered as a slave, when you come back.
But you decide to treat her like a normal person.

# Chapter 3: Exploit

Back in the city you first relax a bit while looking for a way to learn more about the problems of this city.

...

# Chapter 4: Black markets

After some years living in the city, you almost get used to the comfort there.
As an adult, your childish fantasies about revolution and saving the world get less important.
Everything is fine here.

But then you see find some information about some streamer you watched all these years ago.
First you don't believe it, but you really start to get into it and it seems to be true.
She seems to have left the city and lives somewhere on the ocean now.
You didn't expect that, but that kind of suits her.

You fantasize about going there. It feels exciting.
But you woulnd't really do it. It's too comfortalbe here. And going there would really be dangerous since you heard a lot about pirates terrorizing the people in that area.
You also remember your village being attacked by such pirates when you were young.

But you can't stop thinking about that girl so in the end you decide to go anyway.
To be honest: Life in the city has become kind of lame.
It's the same everyday.
Sometimes you just have to risk something.

So you decide to leave the city and go to the ocean.
You start your journey and walk through the villages and fields until you reach the ocean.

At the port, you find someone to take you with them if you work.
You are not used to such hard work anymore, but since you know it from your childhood, it doesn't take long until you get used to it again.
It's not that bad.

On the ocean you have a few adventures and even a few fights.
But you realize most of the so called pirates arent as bad as you were told.
Most of them are just normal people.

They don't even steal anything, they mostly just trade with each other.
Some get a lot more than others, but still better than stealing.

But you try to find to Monerochan as fast as possible.
You need to ask a few people.
Some people have noticed and still know, what you did at the agriculture center.
They trust you and give you useful information for finding her.
So it doesn't take too long until you reach her place.

You have to use some elevator hidden on a small island to reach her.
She lives in a unterwater city.

In the city, her base is not difficult to find.
You go to her base and are allowed to enter.
And there you see her in person for the first time.

Monerochan is the ruler there and she welcomes you personally.
You go to her and kneel down in front of her.
She asks you if you want to join the revolution, since you seem to be a strong and smart person and could be very helpful.

You agree to help.
They're already planning something.

# Chapter 5: The secret mission

You immediately get involved in their mission.
They are currently planning to infiltrate the city.

It's a difficult mission, but your contacts in the city might help a lot.
Noone currently suspects anything from you.
You have been a normal citicen for so many years.

While planning you make friends with some of the team members and also talk a lot to Monerochan.
After the plan is finished, you set out.

The team has their own ship, which you all use to get back to the main island, where the city is.
You have to stop a few times, but after some time you reach the coast.

There you split into multiple groups.
Your group has to go to the city first.
Monerochan drives her car to get you to the city faster.

You enter the city alone. Most of the team members might already be suspicious.
You have to get them in without anyone noticing.

When everyone is in, you need to kill some of the supposed leaders of this society.
They have a lot of freedoms and only abuse the system for their own needs.

After killing some of them, you find out about some secret place, where they often meet for some disgusting rituals and parties.

So you decide to go there alltogether.
But nothing is there.

When it turns out to be a trap, it's already too late.
Some of your team are killed, some can flee and some are captured.

You are captured and lose your consciousness.

# Chapter 6: Lost

You wake up in some vast empty place.
It's difficult to breathe and don't know, where to go.

Then Monerochan shows up in some oxygen suit and gives you her oxygen.
She seems to be happy she found you alive.

Then you go for a bit until you reach her car and drive away back to your base.

# Chapter 7: Preparations

...

# Chapter 8: Party time

...
