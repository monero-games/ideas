The habitable world is basically one island with three areas (ordered by size):

* a rural area
* a huge city
* a nature area

# City

Very huge city, most of the population lives there.

## Lifestyle

* almost everywhere, at every home and in public places, are different kinds of PCs to play games and watch porn to keep the people happy
* only public transportation, no personal cars or similar

## Wealth

* most people own almost nothig, but are happy
* most people don't have to work, everything is automated
* noone can contribute anything important to society
* people earn some reputation in online communities, mostly by showing their naked bodies

## Political system

* framed as a democratic free system without a government
* some hidden govern controls the popular opinion, though
* everyone also kind of knows about these people with a lot of influence, but almost noone cares
* criticizing the system is possible and is mostly not punished, but most people just don't care and noone has the power to change anything

## Freedom

* total surveillence
* everyone (theoretically) knows everything about everyone
* even if everyone could know anything, the secret government manages to hide or reframe their own crimes while focusing on things their enemies did wrong
* the only way to profit is to use the total surveillence yourself in the same way as the government

# Rural areas

Some villages exist in a rural area.

## Wealth

* people have to work hard to get anything
* they build their houses and plant their food themselves

## Relationship to the City

* many of them just go to the city, so only the most conservative people stay there
* people are kind of expolited if they have something, that does not exist in the city
* people get nice gifts (like TVs and computers), so they don't really realize
* most of the automated agriculture is there but the villagers don't get much of it

# Nature areas

Some people live in nature areas with a lot of forests.

## Lifestyle

* everything is nice there
* not much need to work, edible plants grow everywhere because of some genetically created plants
* a lot of different lifestyles from hippies to cannibals

## Relationship to the City

* people from the city go there for vacation
* people from the city just take people with them as slaves
* people living here almost never go to the city by themselves

# Ocean

* some shady activities take place on the ocean
* free markets only exist there
* pirates sometimes attack villages and the city protects the villages, or at least claim to
* most seafarers are peaceful though and also often are 
* the only place far enough from the city so they government can't control it
* there are some hidden underwater cities unknown by the big city

# Inhabitable areas

* most areas of the planet are not habitable anymore
* almost noone knows why
  * there might have been a huge war, which destroyed everything besides this small island
  * they might just live in a small artificial biome on a newly colonized planet

