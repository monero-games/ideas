Characters are mostly inspired by existing art.

# Monero

* normally called Monerochan
* grew up in the city
* dominant
* appearance
  * orange/gray hair
  * huge but still natural looking breasts
* secretly owns some stuff
  * orange car
  * 3D printed guns
* personality
  * stoic
  * settled
  * extravagant
* outfits
  * orange/gray outfit
  * pirate/seafarer (when visiting the ocean)
  * western (when visiting the villages)
  * beach outfit (when on the beach)
  * space suit (when leaving the habitable area)

## Young Monerochan

* young Monero inspired by Lolinero art
* personality
  * naughty
  * rebellious
  * curious

# Wownero

* called Wownerochan 
* childhood friend and slave of Monero
* not a human, but a partial cat girl, created using genetics, mostly to be a slave to normal humans
* personality
  * submissive
  * crazy
* appearance
  * blonde hair
  * unnaturally huge breasts
  * cat ears instead of normal human ones
  * a tail
* outfits
  * purple outfit, much skin visible

